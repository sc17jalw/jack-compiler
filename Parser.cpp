#include "Token.h"
#include "Parser.h"

namespace Parser {
  void Parser::init() {
    symbolTables[0] = SymbolTable();
    symbolTables[1] = SymbolTable();
    inIfStatement = false;

    // Add in-built library functions to global symbol table.
    // Math class.
    symbolTables[0].addSymbol("Math", "Math", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Math");
    symbolTables[0].addSubroutine("abs", symbolKind(function), 1, "int", "Math");
    symbolTables[0].addSubroutine("multiply", symbolKind(function), 2, "int", "Math");
    symbolTables[0].addSubroutine("divide", symbolKind(function), 2, "int", "Math");
    symbolTables[0].addSubroutine("min", symbolKind(function), 2, "int", "Math");
    symbolTables[0].addSubroutine("max", symbolKind(function), 2, "int", "Math");
    symbolTables[0].addSubroutine("sqrt", symbolKind(function), 1, "int", "Math");

    // Array class.
    symbolTables[0].addSymbol("Array", "Array", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Array");
    symbolTables[0].addSubroutine("new", symbolKind(function), 1, "Array", "Array");
    symbolTables[0].addSubroutine("dispose", symbolKind(method), 0, "void", "Array");

    // Memory class.
    symbolTables[0].addSymbol("Memory", "Memory", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Memory");
    symbolTables[0].addSubroutine("peek", symbolKind(function), 1, "int", "Memory");
    symbolTables[0].addSubroutine("poke", symbolKind(function), 2, "void", "Memory");
    symbolTables[0].addSubroutine("alloc", symbolKind(function), 1, "Array", "Memory");
    symbolTables[0].addSubroutine("deAlloc", symbolKind(function), 1, "void", "Memory");

    // Screen class.
    symbolTables[0].addSymbol("Screen", "Screen", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Screen");
    symbolTables[0].addSubroutine("clearScreen", symbolKind(function), 0, "void", "Screen");
    symbolTables[0].addSubroutine("setColor", symbolKind(function), 1, "void", "Screen");
    symbolTables[0].addSubroutine("drawPixel", symbolKind(function), 2, "void", "Screen");
    symbolTables[0].addSubroutine("drawLine", symbolKind(function), 4, "void", "Screen");
    symbolTables[0].addSubroutine("drawRectangle", symbolKind(function), 4, "void", "Screen");
    symbolTables[0].addSubroutine("drawCircle", symbolKind(function), 3, "void", "Screen");

    // Keyboard class.
    symbolTables[0].addSymbol("Keyboard", "Keyboard", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Keyboard");
    symbolTables[0].addSubroutine("keyPressed", symbolKind(function), 0, "char", "Keyboard");
    symbolTables[0].addSubroutine("readChar", symbolKind(function), 0, "char", "Keyboard");
    symbolTables[0].addSubroutine("readLine", symbolKind(function), 1, "String", "Keyboard");
    symbolTables[0].addSubroutine("readInt", symbolKind(function), 1, "int", "Keyboard");

    // Output class.
    symbolTables[0].addSymbol("Output", "Output", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Output");
    symbolTables[0].addSubroutine("init", symbolKind(function), 0, "void", "Output");
    symbolTables[0].addSubroutine("moveCursor", symbolKind(function), 2, "void", "Output");
    symbolTables[0].addSubroutine("printChar", symbolKind(function), 1, "void", "Output");
    symbolTables[0].addSubroutine("printString", symbolKind(function), 1, "void", "Output");
    symbolTables[0].addSubroutine("printInt", symbolKind(function), 1, "void", "Output");
    symbolTables[0].addSubroutine("println", symbolKind(function), 0, "void", "Output");
    symbolTables[0].addSubroutine("backSpace", symbolKind(function), 0, "void", "Output");

    // String class.
    symbolTables[0].addSymbol("String", "String", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("String");
    symbolTables[0].addSubroutine("new", symbolKind(constructor), 1, "String", "String");
    symbolTables[0].addSubroutine("dispose", symbolKind(method), 0, "void", "String");
    symbolTables[0].addSubroutine("length", symbolKind(method), 0, "int", "String");
    symbolTables[0].addSubroutine("charAt", symbolKind(method), 1, "char", "String");
    symbolTables[0].addSubroutine("setCharAt", symbolKind(method), 2, "void", "String");
    symbolTables[0].addSubroutine("appendChar", symbolKind(method), 1, "String", "String");

    // Sys class.
    symbolTables[0].addSymbol("Sys", "Sys", symbolKind(classDeclaration), "global");
    symbolTables[0].setInitialised("Sys");
    symbolTables[0].addSubroutine("halt", symbolKind(function), 0, "void", "Sys");
    symbolTables[0].addSubroutine("error", symbolKind(function), 1, "void", "Sys");
    symbolTables[0].addSubroutine("wait", symbolKind(function), 1, "void", "Sys");
  }

  void Parser::ok(Lexer::Token t) {
    // Function for debugging, to show that jack code is syntactically correct.
    // std::cout << t.toString() << " is okay.\n";
  }

  void Parser::error(std::string message, Lexer::Token t) {
    // Function is called when an error is found in the jack code.
    std::cout << "\nError:\n" << message << "\nToken lexeme: \"" << t.getLexeme() << "\" - line: " << t.getLineNumber() << "\n";
    outputFile.close();
    exit(0);
  }

  void Parser::parseFile(std::string filename, std::list<std::string> listOfClasses) {
    // Open output file.
    outputFile.open(filename.substr(0, (filename.length() - 5)) + ".vm");
    numberOfFields = 0;

    lexer.init(filename);

    Lexer::Token t = lexer.getNextToken();
    while (t.getTokenType() != Lexer::tokenType(Lexer::endOfFile)) {
      if (t.getLexeme() == "class") {
        ok(t);
      } else {
        error("\"class\" keyword expected", t);
      }

      t = lexer.getNextToken();
      if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
        if (symbolTables[0].findSymbol(t.getLexeme(), "global") == 1) {
          error("Redeclaration of class \"" + t.getLexeme() + "\"", t);
        } else {
          ok(t);
        }
        currentClassName = t.getLexeme();
      } else {
        error("Identifier expected", t);
      }

      t = lexer.getNextToken();
      if (t.getLexeme() == "{") {
        ok(t);
      } else {
        error("\"{\" expected", t);
      }

      t = lexer.peekNextToken();
      while ((t.getLexeme() == "static") || (t.getLexeme() == "field") || (t.getLexeme() == "constructor") || (t.getLexeme() == "function") || (t.getLexeme() == "method")) {
        memberDeclar();
        t = lexer.peekNextToken();
      }

      t = lexer.getNextToken();
      if (t.getLexeme() == "}") {
        ok(t);
      } else {
        error("\"}\" expected.", t);
      }

      // Add new class to global symbol table.
      symbolTables[0].addSymbol(currentClassName, currentClassName, symbolKind(classDeclaration), "global");
      symbolTables[0].setInitialised(currentClassName);

      t = lexer.peekNextToken();
      if (t.getTokenType() != Lexer::tokenType(Lexer::endOfFile)) {
        t = lexer.getNextToken();
      }
    }

    outputFile.close();

    // std::cout << "\t\tGlobal symbol table:\n";
    // symbolTables[0].print();

    // std::cout << "\nCompilation Successful!\n\n";
  }

  void Parser::memberDeclar() {
    Lexer::Token t = lexer.peekNextToken();
    if ((t.getLexeme() == "static") || (t.getLexeme() == "field")) {
      classVarDeclar();
    } else if ((t.getLexeme() == "constructor") || (t.getLexeme() == "function") || (t.getLexeme() == "method")) {
      subRoutineDeclar();
    } else {
      error("Keyword expected", t);
    }
  }

  void Parser::classVarDeclar() {
    Lexer::Token t = lexer.getNextToken();
    symbolKind newVariableKind;
    if (t.getLexeme() == "static") {
      ok(t);
      newVariableKind = symbolKind(staticVariable);
    } else if (t.getLexeme() == "field") {
      ok(t);
      newVariableKind = symbolKind(field);
      numberOfFields++;
    } else {
      error("\"static\" or \"field\" expected", t);
    }

    std::string newVariableType = lexer.peekNextToken().getLexeme();
    type();

    t = lexer.getNextToken();
    if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
      if (symbolTables[currentSymbolTable].findSymbol(t.getLexeme(), currentClassName) == 1) {
        error("Redeclaration of variable", t);
      } else {
        ok(t);
        // Add new variable to symbol table.
        symbolTables[currentSymbolTable].addSymbol(t.getLexeme(), newVariableType, newVariableKind, currentClassName);
      }
    }

    t = lexer.peekNextToken();
    while (t.getLexeme() == ",") {
      t = lexer.getNextToken();
      t = lexer.getNextToken();
      if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
        if (symbolTables[currentSymbolTable].findSymbol(t.getLexeme(), currentClassName) == 1) {
          error("Redeclaration of variable", t);
        } else {
          ok(t);
          // Add new variable to symbol table.
          symbolTables[currentSymbolTable].addSymbol(t.getLexeme(), newVariableType, newVariableKind, currentClassName);
        }
      }
      t = lexer.peekNextToken();
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == ";") {
      ok(t);
    } else {
      error("\";\" expected", t);
    }
  }

  void Parser::type() {
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "int") {
      ok(t);
    } else if (t.getLexeme() == "char") {
      ok(t);
    } else if (t.getLexeme() == "boolean") {
      ok(t);
    } else if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
      ok(t);
    } else {
      error("type expected", t);
    }
  }

  void Parser::subRoutineDeclar() {
    // Change to local symbol table.
    currentSymbolTable = 1;
    // Clear current symbol table.
    symbolTables[1] = SymbolTable();
    symbolTables[1].init();

    symbolKind newKind;
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "constructor") {
      ok(t);
      newKind = symbolKind(constructor);
    } else if (t.getLexeme() == "function") {
      ok(t);
      newKind = symbolKind(function);
    } else if (t.getLexeme() == "method") {
      ok(t);
      newKind = symbolKind(method);
      symbolTables[1].addSymbol("this", "reference", symbolKind(argument), currentClassName);
    } else {
      error("Keyword expected", t);
    }

    t = lexer.peekNextToken();
    std::string newReturnType = t.getLexeme();
    if (t.getLexeme() == "void") {
      ok(t);
      t = lexer.getNextToken();
    } else {
      type();
    }

    t = lexer.peekNextToken();
    std::string newSubroutineName = t.getLexeme();
    if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
      ok(t);
      t = lexer.getNextToken();
      t = lexer.peekNextToken();
      if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
        ok(t);
        t = lexer.getNextToken();
      }
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "(") {
      ok(t);
    } else {
      error("\"(\" expected", t);
    }

    int newNumberOfArguments = paramList();

    t = lexer.getNextToken();
    if (t.getLexeme() == ")") {
      ok(t);
    } else {
      error("\")\" expected", t);
    }

    if ((symbolTables[0].findSymbol(newSubroutineName, currentClassName) == 1) || (symbolTables[1].findSymbol(newSubroutineName, currentClassName) == 1)) {
      error("Redeclaration of subroutine", t);
    } else {
      ok(t);
      // Add new variable to global symbol table.
      symbolTables[0].addSubroutine(newSubroutineName, newKind, newNumberOfArguments, newReturnType, currentClassName);
    }

    // Write to output file.
    subroutineContent = "";
    int numberOfLocalVariables = subRoutineBody();
    outputFile << "function " << currentClassName << "." << newSubroutineName << " " << numberOfLocalVariables << "\n";
    if (newKind == symbolKind(constructor)) {
      // VM Code for constructor should include allocating the memory.
      outputFile << "push constant " + std::to_string(numberOfFields) +"\n";
      outputFile << "call Memory.alloc 1\n";
      outputFile << "pop pointer 0\n";
    } else if (newKind == symbolKind(method)){
      outputFile << "push argument 0\n";
      outputFile << "pop pointer 0\n";
    }
    outputFile << subroutineContent;
  }


  int Parser::paramList() {
    int noOfArguments = 0;
    Lexer::Token t = lexer.peekNextToken();
    if (t.getTokenType() != Lexer::tokenType(Lexer::symbol)) {
      noOfArguments++;
      std::string newVariableType = lexer.peekNextToken().getLexeme();
      type();

      t = lexer.getNextToken();
      if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
        if ((symbolTables[0].findSymbol(t.getLexeme(), currentClassName) == 1) || (symbolTables[1].findSymbol(t.getLexeme(), currentClassName) == 1)) {
          error("Redeclaration of variable", t);
        } else {
          ok(t);
          // Add new variable to symbol table.
          symbolTables[currentSymbolTable].addSymbol(t.getLexeme(), newVariableType, symbolKind(argument), currentClassName);
          symbolTables[currentSymbolTable].setInitialised(t.getLexeme());
        }
      } else {
        error("Identifier expected", t);
      }

      t = lexer.peekNextToken();
      while (t.getLexeme() == ",") {
        noOfArguments++;
        t = lexer.getNextToken();
        newVariableType = lexer.peekNextToken().getLexeme();
        type();
        t = lexer.getNextToken();
        if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
          if ((symbolTables[0].findSymbol(t.getLexeme(), currentClassName) == 1) || (symbolTables[1].findSymbol(t.getLexeme(), currentClassName) == 1)) {
            error("Redeclaration of variable", t);
          } else {
            ok(t);
            // Add new variable to symbol table.
            symbolTables[currentSymbolTable].addSymbol(t.getLexeme(), newVariableType, symbolKind(argument), currentClassName);
            symbolTables[currentSymbolTable].setInitialised(t.getLexeme());
          }
        } else {
          error("Identifier expected", t);
        }
        t = lexer.peekNextToken();
      }
    }
    return noOfArguments;
  }

  int Parser::subRoutineBody() {
    int numberOfLocalVariables = 0;
    returnProvided = false;
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "{") {
      ok(t);
    } else {
      error("\"{\" expected", t);
    }

    t = lexer.peekNextToken();
    while ((t.getLexeme() == "var") || (t.getLexeme() == "let") || (t.getLexeme() == "if") || (t.getLexeme() == "while") || (t.getLexeme() == "do") || (t.getLexeme() == "return")) {
      if (returnProvided == true) {
        error("The \"" + t.getLexeme() + "\" statement is unreachable because the subroutine has already returned at this point", t);
      }
      numberOfLocalVariables += statement();
      t = lexer.peekNextToken();
    }

    // std::cout << "\n\t\tLocal symbol table:\n";
    // symbolTables[1].print();
    // std::cout << "\n";
    currentSymbolTable = 0;
    t = lexer.getNextToken();
    if (t.getLexeme() == "}") {
      ok(t);
    } else {
      error("\"}\" expected", t);
    }

    // Check that the function contains a non conditional return statement.
    if (returnProvided == false) {
      error("The subroutine does not contain a non conditional return statement", t);
    }
    return numberOfLocalVariables;
  }

  int Parser::statement() {
    int numberOfVars = 0;
    Lexer::Token t = lexer.peekNextToken();
    if (t.getLexeme() == "var") {
       numberOfVars = varDeclarStatement();
    } else if (t.getLexeme() == "let") {
      letStatement();
    } else if (t.getLexeme() == "if") {
      ifStatement();
    } else if (t.getLexeme() == "while") {
      whileStatement();
    } else if (t.getLexeme() == "do") {
      doStatement();
    } else if (t.getLexeme() == "return") {
      returnStatement();
    } else {
      error("Keyword expected", t);
    }
    return numberOfVars;
  }

  int Parser::varDeclarStatement() {
    int numberOfVars = 1;
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "var") {
      ok(t);
    } else {
      error("\"var\" expected", t);
    }

    std::string newVariableType = lexer.peekNextToken().getLexeme();
    type();

    t = lexer.getNextToken();
    if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
      if ((symbolTables[0].findSymbol(t.getLexeme(), currentClassName) == 1) || (symbolTables[1].findSymbol(t.getLexeme(), currentClassName) == 1)) {
        error("Redeclaration of variable", t);
      } else {
        ok(t);
        // Add new variable to symbol table.
        symbolTables[currentSymbolTable].addSymbol(t.getLexeme(), newVariableType, symbolKind(var), currentClassName);
      }
    } else {
      error("Identifier expected", t);
    }

    t = lexer.peekNextToken();
    while (t.getLexeme() == ",") {
      numberOfVars++;
      t = lexer.getNextToken();
      t = lexer.getNextToken();
      if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
        if ((symbolTables[0].findSymbol(t.getLexeme(), currentClassName) == 1) || (symbolTables[1].findSymbol(t.getLexeme(), currentClassName) == 1)) {
          error("Redeclaration of variable", t);
        } else {
          ok(t);
          // Add new variable to symbol table.
          symbolTables[currentSymbolTable].addSymbol(t.getLexeme(), newVariableType, symbolKind(var), currentClassName);
        }
      } else {
        error("Identifier expected after \",\"", t);
      }
      t = lexer.peekNextToken();
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == ";") {
      ok(t);
    } else {
      error("\";\" expected after variable declaration statement", t);
    }
    return numberOfVars;
  }

  void Parser::letStatement() {
    bool toArray = false;
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "let") {
      ok(t);
    } else {
      error("\"let\" expected at start of let statement", t);
    }

    std::string typeExpected;
    t = lexer.getNextToken();
    std::string variableAssigned = t.getLexeme();
    if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {

      if (symbolTables[1].findSymbol(t.getLexeme(), currentClassName)) {
        typeExpected = symbolTables[1].getSymbol(t.getLexeme()).getType();
      } else {
        typeExpected = symbolTables[0].getSymbol(t.getLexeme()).getType();
      }
      ok(t);

    } else {
      error("Identifier expected", t);
    }

    t = lexer.peekNextToken();
    if (t.getLexeme() == "[") {
      toArray = true;
      ok(t);
      typeExpected = "int";
      t = lexer.getNextToken();
      std::string arrayIndexReturned = expression();
      if ((arrayIndexReturned != "int") && (arrayIndexReturned != "Array")) {
        error("Expression used as array index does not evaluate to an int", t);
      }
      // Pop array index.
      if (symbolTables[1].findSymbol(variableAssigned, currentClassName) == 1) {
        int offset = symbolTables[1].getSymbol(variableAssigned).getOffset();
        symbolKind variableKind = symbolTables[1].getSymbol(variableAssigned).getKind();
        if (variableKind == symbolKind(var)) {
          subroutineContent += "push local " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(staticVariable)) {
          subroutineContent += "push static " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(field)) {
          subroutineContent += "push this " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(argument)) {
          subroutineContent += "push argument " + std::to_string(offset) + "\n";
        }
      } else {
        int offset = symbolTables[0].getSymbol(variableAssigned).getOffset();
        symbolKind variableKind = symbolTables[0].getSymbol(variableAssigned).getKind();
        if (variableKind == symbolKind(var)) {
          subroutineContent += "push local " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(staticVariable)) {
          subroutineContent += "push static " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(field)) {
          subroutineContent += "push this " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(argument)) {
          subroutineContent += "push argument " + std::to_string(offset) + "\n";
        }
      }
      subroutineContent += "add\n";
      t = lexer.getNextToken();
      if (t.getLexeme() == "]") {
        ok(t);
      } else {
        error("\"]\" expected", t);
      }
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "=") {
      ok(t);
    } else {
      error("\"=\" expected", t);
    }

    std::string typeReturned = expression();
    if ((typeReturned != typeExpected) && (typeReturned != "undeclared") && (typeReturned != "reference")) {
      error("Type returned does not match the type expected", t);
    }


    t = lexer.getNextToken();
    if (t.getLexeme() == ";") {
      ok(t);
    } else {
      error("\";\" expected", t);
    }

    // Write to output file.
    if (toArray == true) {
      // Writing to an array location.
      subroutineContent += "pop temp 0\n";
      subroutineContent += "pop pointer 1\n";
      subroutineContent += "push temp 0\n";
      subroutineContent += "pop that 0\n";
    } else {
      // Writing to a variable on the symbol table.
      if (symbolTables[1].findSymbol(variableAssigned, currentClassName) == 1) {
        int offset = symbolTables[1].getSymbol(variableAssigned).getOffset();
        symbolKind variableKind = symbolTables[1].getSymbol(variableAssigned).getKind();
        if (variableKind == symbolKind(var)) {
          subroutineContent += "pop local " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(staticVariable)) {
          subroutineContent += "pop static " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(field)) {
          subroutineContent += "pop this " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(argument)) {
          subroutineContent += "pop argument " + std::to_string(offset) + "\n";
        }
      } else {
        int offset = symbolTables[0].getSymbol(variableAssigned).getOffset();
        symbolKind variableKind = symbolTables[0].getSymbol(variableAssigned).getKind();
        if (variableKind == symbolKind(var)) {
          subroutineContent += "pop local " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(staticVariable)) {
          subroutineContent += "pop static " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(field)) {
          subroutineContent += "pop this " + std::to_string(offset) + "\n";
        } else if (variableKind == symbolKind(argument)) {
          subroutineContent += "pop argument " + std::to_string(offset) + "\n";
        }
      }


      // Set the variable give a value as initialised.
      if (symbolTables[1].findSymbol(variableAssigned, currentClassName) == 1) {
        symbolTables[1].setInitialised(variableAssigned);
      } else {
        symbolTables[0].setInitialised(variableAssigned);
      }
    }
  }

  void Parser::ifStatement() {
    bool inParentIfStatement;
    if (inIfStatement == true) {
      inParentIfStatement = false;
    } else {
      inParentIfStatement = true;
    }
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "if") {
      ok(t);
    } else {
      error("\"if\" expected at start of if statement", t);
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "(") {
      ok(t);
    } else {
      error("\"(\" expected", t);
    }

    expression();

    t = lexer.getNextToken();
    if (t.getLexeme() == ")") {
      ok(t);
    } else {
      error("\")\" expected", t);
    }

    subroutineContent += "not\n";
    std::string lineID = std::to_string(t.getLineNumber());
    subroutineContent += "if-goto end" + lineID + "\n";

    t = lexer.getNextToken();
    if (t.getLexeme() == "{") {
      ok(t);
    } else {
      error("\"{\" expected", t);
    }

    // The parser is now in the body of an if statement.
    inIfStatement = true;

    t = lexer.peekNextToken();
    while ((t.getLexeme() == "var") || (t.getLexeme() == "let") || (t.getLexeme() == "if") || (t.getLexeme() == "while") || (t.getLexeme() == "do") || (t.getLexeme() == "return")) {
      statement();
      t = lexer.peekNextToken();
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "}") {
      ok(t);
    } else {
      error("\"}\" expected", t);
    }



    subroutineContent += "label end" + lineID + "\n";

    t = lexer.peekNextToken();
    if (t.getLexeme() == "else") {
      ok(t);
      t = lexer.getNextToken();
      t = lexer.getNextToken();
      if (t.getLexeme() == "{") {
        ok(t);
      } else {
        error("\"{\" expected", t);
      }

      // The parser is now in the body of an if statement.
      inIfStatement = true;

      t = lexer.peekNextToken();
      while ((t.getLexeme() == "var") || (t.getLexeme() == "let") || (t.getLexeme() == "if") || (t.getLexeme() == "while") || (t.getLexeme() == "do") || (t.getLexeme() == "return")) {
        statement();
        t = lexer.peekNextToken();
      }

      if (inParentIfStatement == true) {
        // The parser is now out of the if statement.
        inIfStatement = false;
      }

      t = lexer.getNextToken();
      if (t.getLexeme() == "}") {
        ok(t);
      } else {
        error("\"}\" expected", t);
      }
    }
    if (inParentIfStatement == true) {
      // The parser is now out of the if statement.
      inIfStatement = false;
    }
  }

  void Parser::whileStatement() {
    bool inParentIfStatement;
    if (inIfStatement == true) {
      inParentIfStatement = false;
    } else {
      inParentIfStatement = true;
    }

    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "while") {
      ok(t);
    } else {
      error("\"while\" expected at start of while statement", t);
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "(") {
      ok(t);
    } else {
      error("\"(\" expected", t);
    }

    std::string lineID = std::to_string(t.getLineNumber());
    subroutineContent += "label loop" + lineID + "\n";

    expression();

    t = lexer.getNextToken();
    if (t.getLexeme() == ")") {
      ok(t);
    } else {
      error("\")\" expected", t);
    }

    subroutineContent += "not\n";
    subroutineContent += "if-goto end" + lineID + "\n";

    t = lexer.getNextToken();
    if (t.getLexeme() == "{") {
      ok(t);
    } else {
      error("\"{\" expected", t);
    }

    // The parser is now inside a while statement.
    inIfStatement = true;

    t = lexer.peekNextToken();
    while ((t.getLexeme() == "var") || (t.getLexeme() == "let") || (t.getLexeme() == "if") || (t.getLexeme() == "while") || (t.getLexeme() == "do") || (t.getLexeme() == "return")) {
      statement();
      t = lexer.peekNextToken();
    }

    if (inParentIfStatement = true) {
      // The parser is now outside the while statement.
      inIfStatement = false;
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "}") {
      ok(t);
    } else {
      error("\"}\" expected", t);
    }

    subroutineContent += "goto loop" + lineID + "\n";
    subroutineContent += "label end" + lineID + "\n";
  }

  void Parser::doStatement() {
    Lexer::Token t = lexer.getNextToken();
    if (t.getLexeme() == "do") {
      ok(t);
    } else {
      error("\"do\" expected at start of do statement", t);
    }

    subRoutineCall();
    subroutineContent += "pop temp 0\n";
  }

  void Parser::subRoutineCall() {
    std::string parentClassName = "";
    std::string subroutineName;
    bool subroutineDeclared = true;
    Symbol subroutineSymbol = Symbol(symbolKind(field), "empty", "empty", -1, "empty");
    Lexer::Token t = lexer.getNextToken();
    std::string classCall = t.getLexeme();
    if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
      subroutineName = t.getLexeme();
      ok(t);
    } else {
      error("Identifier expected at start of subroutine call", t);
    }

    t = lexer.peekNextToken();
    if (t.getLexeme() == ".") {
      ok(t);
      t = lexer.getNextToken();
      t = lexer.getNextToken();
      if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {
        parentClassName = subroutineName;
        if (parentClassName == "this") {
          parentClassName = currentClassName;
        }
        subroutineName = t.getLexeme();
        if (symbolTables[1].findSubRoutine(subroutineName, parentClassName) == true) {
          ok(t);
          subroutineSymbol = symbolTables[1].getSubRoutine(subroutineName, parentClassName);
        } else if (symbolTables[0].findSubRoutine(subroutineName, parentClassName) == 1) {
          ok(t);
          subroutineSymbol = symbolTables[0].getSubRoutine(subroutineName, parentClassName);
        } else {
          // error("There is no existing subroutine: " + subroutineName + " with the parent class: " + parentClassName, t);
          subroutineDeclared = false;
        }
      } else {
        error("Identifier expected after \".\"", t);
      }
    } else {
      if (symbolTables[1].findSubRoutine(subroutineName, currentClassName) == 1) {
        ok(t);
        subroutineSymbol = symbolTables[1].getSubRoutine(subroutineName, currentClassName);
      } else if (symbolTables[0].findSubRoutine(subroutineName, currentClassName) == 1) {
        ok(t);
        subroutineSymbol = symbolTables[0].getSubRoutine(subroutineName, currentClassName);
      } else {
        // error("There is no existing subroutine: " + subroutineName + " with the parent class: " + currentClassName, t);
        subroutineDeclared = false;
      }
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == "(") {
      ok(t);
    } else {
      error("\"(\" expected", t);
    }
    bool isMethod = false;
    std::string callingClass = "";
    if ((parentClassName == "") || (parentClassName == "this")) {
      // Subroutine call is for a static object.
      callingClass = currentClassName;
      if ((symbolTables[1].findSubRoutine(subroutineName, callingClass) == true)) {
        isMethod = true;
        // Subroutine declared on symbol table [1].
        if (symbolTables[1].getSubRoutine(subroutineName, callingClass).getKind() == symbolKind(method)) {
          subroutineContent += "push pointer 0\n";
        }
      } else if (symbolTables[0].findSubRoutine(subroutineName, callingClass) == true) {
        isMethod = true;
        // Subroutine declared on symbol table [0].
        if (symbolTables[0].getSubRoutine(subroutineName, callingClass).getKind() == symbolKind(method)) {
          subroutineContent += "push pointer 0\n";
        }
      }
    } else {
      // Two identifiers are given.
      if (((symbolTables[1].findSymbol(parentClassName) == true) && (symbolTables[1].getSymbol(parentClassName).getKind() != symbolKind(classDeclaration))) || ((symbolTables[0].findSymbol(parentClassName) == true) && (symbolTables[0].getSymbol(parentClassName).getKind() != symbolKind(classDeclaration)))) {
        // If subroutine call is a method.
        isMethod = true;
        if (symbolTables[1].findSymbol(parentClassName) == true) {
          // On symbol table 1.
          Symbol symbolToPush = symbolTables[1].getSymbol(parentClassName);
          if (symbolToPush.getKind() == symbolKind(var)) {
            subroutineContent += "push local " + std::to_string(symbolToPush.getOffset()) + "\n";
          } else if (symbolToPush.getKind() == symbolKind(argument)) {
            subroutineContent += "push argument " + std::to_string(symbolToPush.getOffset()) + "\n";
          } else if (symbolToPush.getKind() == symbolKind(field)) {
            subroutineContent += "push this " + std::to_string(symbolToPush.getOffset()) + "\n";
          } else if (symbolToPush.getKind() == symbolKind(staticVariable)) {
            subroutineContent += "push static " + std::to_string(symbolToPush.getOffset()) + "\n";
          }
          callingClass = symbolToPush.getType();
        } else {
          // On symbol table 0.
          Symbol symbolToPush = symbolTables[0].getSymbol(parentClassName);
          if (symbolToPush.getKind() == symbolKind(var)) {
            subroutineContent += "push local " + std::to_string(symbolToPush.getOffset()) + "\n";
          } else if (symbolToPush.getKind() == symbolKind(argument)) {
            subroutineContent += "push argument " + std::to_string(symbolToPush.getOffset()) + "\n";
          } else if (symbolToPush.getKind() == symbolKind(field)) {
            subroutineContent += "push this " + std::to_string(symbolToPush.getOffset()) + "\n";
          } else if (symbolToPush.getKind() == symbolKind(staticVariable)) {
            subroutineContent += "push static " + std::to_string(symbolToPush.getOffset()) + "\n";
          }
          callingClass = symbolToPush.getType();
        }
      } else {
        // Subroutine call is a function.
        // if (symbolTables[1].findSymbol(parentClassName) == true) {
        //   // On symbol table 1.
        //   Symbol symbolToPush = symbolTables[1].getSymbol(parentClassName);
        //   callingClass = symbolToPush.getType();
        // } else {
        //   // On symbol table 2.
        //   Symbol symbolToPush = symbolTables[0].getSymbol(parentClassName);
        //   callingClass = symbolToPush.getType();
        // }
        callingClass = parentClassName;
      }
    }

    int expressionNumber = expressionList();
    if (subroutineDeclared == true) {
      // Check that the number of expressions matches the number expected.
      if (expressionNumber != subroutineSymbol.getNoArguments()) {
        error("Incorrect number of arguments provided, you gave: " + std::to_string(expressionNumber) + " and: " + std::to_string(subroutineSymbol.getNoArguments()) + " were expected", t);
      }
    }
    if (isMethod == false) {
      subroutineContent += "call " + callingClass + "." + subroutineName + " " + std::to_string(expressionNumber) + "\n";
    } else {
      subroutineContent += "call " + callingClass + "." + subroutineName + " " + std::to_string(expressionNumber + 1) + "\n";
    }


    t = lexer.getNextToken();
    if (t.getLexeme() == ")") {
      ok(t);
    } else {
      error("\")\" expected", t);
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == ";") {
      ok(t);
    } else {
      error("\";\" expected", t);
    }
  }

  int Parser::expressionList() {
    int expressionNumber = 0;
    Lexer::Token t = lexer.peekNextToken();
    // If t is: - ~ integer identifier ( stringConstant true false null this
    if ((t.getLexeme() == "-") || (t.getLexeme() == "~")
        || (t.getTokenType() == Lexer::tokenType(Lexer::integer))
        || (t.getTokenType() == Lexer::tokenType(Lexer::identifier))
        || (t.getLexeme() == "(")
        || (t.getTokenType() == Lexer::tokenType(Lexer::stringConstant))
        || (t.getLexeme() == "true") || (t.getLexeme() == "false")
        || (t.getLexeme() == "null") || (t.getLexeme() == "this")) {
      expressionNumber++;
      expression();

      t = lexer.peekNextToken();
      while (t.getLexeme() == ",") {
        expressionNumber++;
        t = lexer.getNextToken();
        expression();
        t = lexer.peekNextToken();
      }
    }
    return expressionNumber;
  }

  void Parser::returnStatement() {
    Lexer::Token t = lexer.getNextToken();
    // Check that the return statement is not inside an if statement.
    if (inIfStatement == false) {
      returnProvided = true;
    }

    if (t.getLexeme() == "return") {
      ok(t);
    } else {
      error("\"return\" expected at start of return statement", t);
    }

    t = lexer.peekNextToken();
    // If t is: - ~ integer identifier ( stringConstant true false null this
    if ((t.getLexeme() == "-") || (t.getLexeme() == "~")
        || (t.getTokenType() == Lexer::tokenType(Lexer::integer))
        || (t.getTokenType() == Lexer::tokenType(Lexer::identifier))
        || (t.getLexeme() == "(")
        || (t.getTokenType() == Lexer::tokenType(Lexer::stringConstant))
        || (t.getLexeme() == "true") || (t.getLexeme() == "false")
        || (t.getLexeme() == "null") || (t.getLexeme() == "this")) {
      expression();
    } else {
      subroutineContent += "push constant 0\n";
    }

    t = lexer.getNextToken();
    if (t.getLexeme() == ";") {
      ok(t);
    } else {
      error("\";\" expected", t);
    }
    subroutineContent += "return\n";
  }

  std::string Parser::expression() {
    std::string returnType = relationalExpression();

    Lexer::Token t = lexer.peekNextToken();
    while ((t.getLexeme() == "&") || (t.getLexeme() == "|")) {
      t = lexer.getNextToken();
      relationalExpression();
      returnType = "boolean";
      if (t.getLexeme() == "&") {
        subroutineContent += "and\n";
      } else {
        subroutineContent += "or\n";
      }
      t = lexer.peekNextToken();
    }
    return returnType;
  }

  std::string Parser::relationalExpression() {
    std::string returnType = arithmeticExpression();

    Lexer::Token t = lexer.peekNextToken();
    while ((t.getLexeme() == "=") || (t.getLexeme() == ">") || (t.getLexeme() == "<")) {
      ok(t);
      t = lexer.getNextToken();
      arithmeticExpression();
      returnType = "int";
      if (t.getLexeme() == "=") {
        subroutineContent += "eq\n";
      } else if (t.getLexeme() == ">") {
        subroutineContent += "gt\n";
      } else {
        subroutineContent += "lt\n";
      }
      t = lexer.peekNextToken();
    }
    return returnType;
  }

  std::string Parser::arithmeticExpression() {
    std::string returnType = term();

    Lexer::Token t = lexer.peekNextToken();
    while ((t.getLexeme() == "+") || (t.getLexeme() == "-")) {
      ok(t);
      t = lexer.getNextToken();
      term();
      returnType = "int";
      if (t.getLexeme() == "+") {
        subroutineContent += "add\n";
      } else {
        subroutineContent += "sub\n";
      }
      t = lexer.peekNextToken();
    }
    return returnType;
  }

  std::string Parser::term() {
    std::string returnType = factor();

    Lexer::Token t = lexer.peekNextToken();
    while ((t.getLexeme() == "*") || (t.getLexeme() == "/")) {
      ok(t);
      t = lexer.getNextToken();
      factor();
      returnType = "int";
      if (t.getLexeme() == "*") {
        subroutineContent += "call Math.multiply 2\n";
      } else {
        subroutineContent += "call Math.divide 2\n";
      }
      t = lexer.peekNextToken();
    }
    return returnType;
  }

  std::string Parser::factor() {
    Lexer::Token t = lexer.peekNextToken();
    std::string negationOperator = "";
    if (t.getLexeme() == "-") {
      ok(t);
      t = lexer.getNextToken();
      negationOperator = t.getLexeme();
    } else if (t.getLexeme() == "~") {
      ok(t);
      t = lexer.getNextToken();
      negationOperator = t.getLexeme();
    }

    std::string opperandReturn = opperand();

    if (negationOperator == "-") {
      subroutineContent += "neg\n";
    } else if (negationOperator == "~") {
      subroutineContent += "not\n";
    }
    return opperandReturn;
  }

  std::string Parser::opperand() {
    int numberOfExpressionsNeeded, numberOfExpressionsGiven;
    numberOfExpressionsNeeded = -1;
    std::string returnType;
    Lexer::Token t = lexer.getNextToken();
    if (t.getTokenType() == Lexer::tokenType(Lexer::integer)) {
      returnType = "int";
      ok(t);
      subroutineContent += "push constant " + t.getLexeme() + "\n";
    } else if (t.getTokenType() == Lexer::tokenType(Lexer::identifier)) {

      std::string firstIdentifier, secondIdentifier;
      bool subroutineCall = false;
      bool isArray = false;
      int numberOfArgumentsGiven;
      firstIdentifier = t.getLexeme();
      secondIdentifier = "";
      t = lexer.peekNextToken();
      if (t.getLexeme() == ".") {
        t = lexer.getNextToken();
        t = lexer.getNextToken();
        secondIdentifier = t.getLexeme();
      }
      t = lexer.peekNextToken();
      if (t.getLexeme() == "[") {
        returnType = "int";
        t = lexer.getNextToken();
        // Check expression in [] evaluates to an integer.
        if (expression() != "int") {
          error("Expression inside array index must evaluate to an integer", t);
        }
        // subroutineContent += "add\n";
        // subroutineContent += "pop pointer 1\n";
        // subroutineContent += "push that 0\n";
        isArray = true;
        t = lexer.getNextToken();
      } else if (t.getLexeme() == "(") {
        subroutineCall = true;
        t = lexer.getNextToken();

        numberOfArgumentsGiven = expressionList();
        t = lexer.getNextToken();
      }
      // Generate code.
      if (subroutineCall == false) {
        // Is a symbol.
        if (secondIdentifier == "") {
          // There is one identifier.
          Symbol symbol(symbolKind(field), "", "", 0, "");
          if (symbolTables[1].findSymbol(firstIdentifier) == true) {
            symbol = symbolTables[1].getSymbol(firstIdentifier);
          } else if (symbolTables[0].findSymbol(firstIdentifier) == true) {
            symbol = symbolTables[0].getSymbol(firstIdentifier);
          } else {
            error("Symbol \"" + firstIdentifier + "\" is undeclared", t);
          }
          if (symbol.getInitialised() == false) {
            std::cout << "Symbol \"" + firstIdentifier + "\" is uninitialised", t;
          }
          returnType = symbol.getType();
          // Generate push code.
          if (symbol.getKind() == symbolKind(var)) {
            subroutineContent += "push local " + std::to_string(symbol.getOffset()) + "\n";
          } else if (symbol.getKind() == symbolKind(argument)) {
            subroutineContent += "push argument " + std::to_string(symbol.getOffset()) + "\n";
          } else if (symbol.getKind() == symbolKind(field)) {
            subroutineContent += "push this " + std::to_string(symbol.getOffset()) + "\n";
          } else if (symbol.getKind() == symbolKind(staticVariable)) {
            subroutineContent += "push static " + std::to_string(symbol.getOffset()) + "\n";
          }
        } else {
          // There are two identifiers.
          Symbol symbol(symbolKind(field), "", "", 0, "");
          if (symbolTables[1].findSymbol(firstIdentifier) == true) {
            symbol = symbolTables[1].getSymbol(firstIdentifier);
            returnType = symbol.getType();
          } else if (symbolTables[0].findSymbol(firstIdentifier) == true) {
            symbol = symbolTables[0].getSymbol(firstIdentifier);
            returnType = symbol.getType();
          } else {
            // error("Symbol \"" + firstIdentifier + "\" is undeclared", t);
            returnType = "undeclared";
          }
          subroutineContent += "push this " + std::to_string(symbol.getOffset()) + "\n";
        }
        if (isArray == true) {
          subroutineContent += "add\n";
          subroutineContent += "pop pointer 1\n";
          subroutineContent += "push that 0\n";
        }
      } else {
        // Is a subroutine call.
        if (secondIdentifier == "") {
          // There is one identifier.
          if (symbolTables[1].findSubRoutine(firstIdentifier, currentClassName) == true) {
            // Subroutine has been declared in [1].
            Symbol subroutine = symbolTables[1].getSubRoutine(firstIdentifier, currentClassName);
            returnType = subroutine.getType();
            if (numberOfArgumentsGiven != subroutine.getNoArguments()) {
              error("Incorrect number of arguments given, you gave " + std::to_string(numberOfArgumentsGiven) + " and " + std::to_string(subroutine.getNoArguments()) + " were expected", t);
            }
            subroutineContent += "call " + currentClassName + "." + firstIdentifier + " " + std::to_string(numberOfArgumentsGiven) + "\n";
          } else if (symbolTables[0].findSubRoutine(firstIdentifier, currentClassName) == true) {
            // Subroutine has been declared in [0].
            Symbol subroutine = symbolTables[0].getSubRoutine(firstIdentifier, currentClassName);
            returnType = subroutine.getType();
            if (numberOfArgumentsGiven != subroutine.getNoArguments()) {
              error("Incorrect number of arguments given, you gave " + std::to_string(numberOfArgumentsGiven) + " and " + std::to_string(subroutine.getNoArguments()) + " were expected", t);
            }
            subroutineContent += "call " + currentClassName + "." + firstIdentifier + " " + std::to_string(numberOfArgumentsGiven) + "\n";
          } else {
            // Subroutine has not been declared.
            returnType = "undeclared";
            subroutineContent += "call " + currentClassName + "." + firstIdentifier + " " + std::to_string(numberOfArgumentsGiven) + "\n";
          }
        } else {
          // There are two identifiers.
          if (symbolTables[1].findSubRoutine(secondIdentifier, symbolTables[1].getSymbol(firstIdentifier).getType()) == true) {
            // Subroutine has been declared in [1].
            Symbol subroutine = symbolTables[1].getSubRoutine(secondIdentifier, symbolTables[1].getSymbol(firstIdentifier).getType());
            returnType = subroutine.getType();
            if (numberOfArgumentsGiven != subroutine.getNoArguments()) {
              error("Incorrect number of arguments given, you gave " + std::to_string(numberOfArgumentsGiven) + " and " + std::to_string(subroutine.getNoArguments()) + " were expected", t);
            }
            subroutineContent += "call " + symbolTables[1].getSymbol(firstIdentifier).getType() + "." + secondIdentifier + " " + std::to_string(numberOfArgumentsGiven) + "\n";
          } else if (symbolTables[0].findSubRoutine(secondIdentifier, symbolTables[0].getSymbol(firstIdentifier).getType()) == true) {
            // Subroutine has been declared in [0].
            Symbol subroutine = symbolTables[0].getSubRoutine(secondIdentifier, symbolTables[0].getSymbol(firstIdentifier).getType());
            returnType = subroutine.getType();
            if (numberOfArgumentsGiven != subroutine.getNoArguments()) {
              error("Incorrect number of arguments given, you gave " + std::to_string(numberOfArgumentsGiven) + " and " + std::to_string(subroutine.getNoArguments()) + " were expected", t);
            }
            subroutineContent += "call " + symbolTables[0].getSymbol(firstIdentifier).getType() + "." + secondIdentifier + " " + std::to_string(numberOfArgumentsGiven) + "\n";
          } else {
            // Subroutine has not been declared.
            returnType = "undeclared";
            subroutineContent += "call " + firstIdentifier + "." + secondIdentifier + " " + std::to_string(numberOfArgumentsGiven) + "\n";
          }
        }
      }

    } else if (t.getLexeme() == "(") {
      ok(t);
      expression();
      t = lexer.getNextToken();
      if (t.getLexeme() == ")") {
        ok(t);
      } else {
        error("\")\" expected", t);
      }
    } else if (t.getTokenType() == Lexer::tokenType(Lexer::stringConstant)) {
      returnType = "String";
      ok(t);
      subroutineContent += "push constant " + std::to_string(t.getLexeme().length()) + "\n";
      subroutineContent += "call String.new 1\n";
      for (int i = 0; i < t.getLexeme().length(); i++) {
        subroutineContent += "push constant " + std::to_string((int) t.getLexeme().at(i)) + "\n";
        subroutineContent += "call String.appendChar 2\n";
      }
    } else if (t.getLexeme() == "true") {
      returnType = "boolean";
      ok(t);
      subroutineContent += "push constant 0\nnot\n";
    } else if (t.getLexeme() == "false") {
      returnType = "boolean";
      ok(t);
      subroutineContent += "push constant 0\n";
    } else if (t.getLexeme() == "null") {
      returnType = "null";
      ok(t);
      subroutineContent += "push constant 0\n";
    } else if (t.getLexeme() == "this") {
      returnType = "reference";
      ok(t);
      subroutineContent += "push pointer 0\n";
    }
    return returnType;
  }
}
