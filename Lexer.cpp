#include "Token.h"
#include "Lexer.h"
#include <iostream>
#include <fstream>
#include <cctype>

namespace Lexer {
  void Lexer::init(std::string filename) {
    inputFile.open(filename);
    if (!inputFile.is_open()) {
      std::cout << "File " << filename << " does not exist.\n";
      exit(0);
    }
    currentLine = 1;
    tokenComputed = 0;
  }

  void Lexer::closeFile() {
    inputFile.close();
  }

  Token Lexer::peekNextToken() {
    if (tokenComputed == 1) {
      // Token has already been computed.
      return computedToken;
    } else {
      // Token has not yet been computed.
      computedToken = getNextToken();
      tokenComputed = 1;
      return computedToken;
    }
  }

  Token Lexer::getNextToken() {
    if (tokenComputed == 1) {
      // Token has already been computed.
      tokenComputed = 0;
      return computedToken;

    } else if (tokenComputed == 0) {

      // Token has not yet been computed.
      int inputChar;
      while (1 == 1) {
        // Move through whitespace.
        inputChar = inputFile.peek();
        while ((inputChar != -1) && (isspace((char) inputChar))) {
          if (((char) inputChar) == '\n') {
            currentLine++;
          }
          inputFile.get();
          inputChar = inputFile.peek();
        }

        // Skip comments.
        if (inputChar == '/') {
          inputFile.get();
          inputChar = inputFile.peek();
          if (inputChar == '/') {
            // The comment is a single line comment.
            inputFile.get();
            inputChar = inputFile.peek();
            while (inputChar != '\n') {
              inputFile.get();
              inputChar = inputFile.peek();
            }
          } else if (inputChar == '*') {
            // The comment is a multi line comment.
            inputFile.get();
            while (1 == 1) {
              inputChar = inputFile.peek();
              if (inputChar == '*') {
                inputFile.get();
                inputChar = inputFile.peek();
                if (inputChar == '/') {
                  inputFile.get();
                  break;
                } else {
                  inputFile.unget();
                }
              }
              inputChar = inputFile.peek();
              if (((char) inputChar) == '\n') {
                currentLine++;
              }
              inputFile.get();
            }
            inputChar = inputFile.peek();
          } else {
            // The backslash is a symbol.
            inputFile.get();
            inputChar = inputFile.peek();
            return Token("/", tokenType(symbol), currentLine);
          }
        }

        // Check for EOF character.
        inputChar = inputFile.peek();
        if (inputChar == -1) {
          inputFile.get();
          return Token("", tokenType(endOfFile), currentLine);
        }

        // Check for symbols.
        inputChar = inputFile.peek();
        int isSymbol = 0;
        for (int i = 0; i < 19; ++i) {
          if (inputChar == symbols[i]) {
            isSymbol = 1;
          }
        }
        if (isSymbol == 1) {
          inputFile.get();
          return Token(std::string(1, inputChar), tokenType(symbol), currentLine);
        }

        // Check for digit.
        inputChar = inputFile.peek();
        if (isdigit((char) inputChar)) {
          std::string newLexeme = "";
          newLexeme += inputChar;
          inputFile.get();
          inputChar = inputFile.peek();
          while (isdigit((char) inputChar)) {
            newLexeme += inputChar;
            inputFile.get();
            inputChar = inputFile.peek();
          }
          return Token(newLexeme, tokenType(integer), currentLine);
        }

        // Check for identifier/keyword.
        inputChar = inputFile.peek();
        if ((isalpha((char) inputChar)) || ((char) inputChar == '_')) {
          std::string newLexeme = "";
          newLexeme += inputChar;
          inputFile.get();
          inputChar = inputFile.peek();
          while (isalpha((char) inputChar) || isdigit((char) inputChar) || ((char) inputChar == '_')) {
            newLexeme += inputChar;
            inputFile.get();
            inputChar = inputFile.peek();
          }
          int isKeyword = 0;
          for (int i = 0; i < 21; ++i) {
            if (newLexeme == keywords[i]) {
              isKeyword = 1;
            }
          }
          if (isKeyword == 1) {
            return Token(newLexeme, tokenType(keyword), currentLine);
          } else {
            return Token(newLexeme, tokenType(identifier), currentLine);
          }
        }

        // Check for string constant.
        inputChar = inputFile.peek();
        if (((char) inputChar) == '"') {
          inputFile.get();
          inputChar = inputFile.peek();
          std::string newLexeme = "";
          while (((char) inputChar) != '"') {
            newLexeme += inputChar;
            inputFile.get();
            inputChar = inputFile.peek();
          }
          inputFile.get();
          return Token(newLexeme, tokenType(stringConstant), currentLine);
        }

        // Check for illegal characters.
        inputChar = inputFile.peek();
        if ((!(isalpha((char) inputChar))) && (isSymbol == 0) && (!(isdigit((char) inputChar))) && (inputChar != -1) && (((char) inputChar) != '"') && (!(isspace((char) inputChar)))) {
          std::cout << "You have entered an illegal character on line " << std::to_string(currentLine) << ": " << (char) inputChar << ".\n";
          exit(1);
        }
      }
    }
    return Token("", tokenType(stringConstant), 0);
  }
}
