// #include <iostream>
#include <fstream>

namespace Lexer {
  class Lexer {
  private:
    std::ifstream inputFile;
    int currentLine;
    Token computedToken = Token("", tokenType(keyword), 0);
    int tokenComputed;
    int symbols[19] = {'{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-',
                        '*', '/', '&', '|', '<', '>', '=', '~'};
    std::string keywords[21] = {"class", "method", "function", "constructor",
                                "int", "boolean", "char", "void",
                                "var", "static", "field",
                                "let", "do", "if", "else", "while", "return",
                                "true", "false", "null", "this"};

  public:
    void init(std::string filename);
    void closeFile();
    Token getNextToken();
    Token peekNextToken();
  };
}
