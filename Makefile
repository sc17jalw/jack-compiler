CXX = g++
CXXFLAGS = -g -std=c++11

# Link object files.
myjc: main.o Lexer.o Parser.o
	$(CXX) $(CXXFLAGS) -o $@ main.o Lexer.o Parser.o

# Compile the main.cpp file.
main.o: main.cpp Token.h Parser.h Lexer.h
	$(CXX) $(CXXFLAGS) -c main.cpp
# Compile the Lexer source code.
Lexer.o: Lexer.cpp Token.h Lexer.h
	$(CXX) $(CXXFLAGS) -c Lexer.cpp
# Compile the Parser source code.
Parser.o: Parser.cpp Token.h Parser.h Lexer.h SymbolTable.h
	$(CXX) $(CXXFLAGS) -c Parser.cpp
