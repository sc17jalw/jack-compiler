// #include "Token.h"
#include "Lexer.h"
#include "SymbolTable.h"
// #include <string>
// #include <iostream>
// #include <fstream>

namespace Parser {
  class Parser {
  private:
    Lexer::Lexer lexer;
    SymbolTable symbolTables[2];
    int currentSymbolTable;
    void error(std::string message, Lexer::Token t);
    void ok(Lexer::Token t);
    void classDeclar();
    void memberDeclar();
    void classVarDeclar();
    void type();
    void subRoutineDeclar();
    int paramList();
    int subRoutineBody();
    int statement();
    int varDeclarStatement();
    void letStatement();
    void ifStatement();
    void whileStatement();
    void doStatement();
    void subRoutineCall();
    int expressionList();
    void returnStatement();
    std::string expression();
    std::string relationalExpression();
    std::string arithmeticExpression();
    std::string term();
    std::string factor();
    std::string opperand();
    bool inIfStatement;
    bool returnProvided;
    std::string currentClassName;
    std::ofstream outputFile;
    std::string subroutineContent;
    int numberOfFields;
  public:
    void parseFile(std::string filename, std::list<std::string> listOfClasses);
    void init();
  };
}
