#include <string>


namespace Lexer {
  enum tokenType {keyword, identifier, symbol, integer, stringConstant, endOfFile};
  class Token {
  private:
    std::string lexeme;
    tokenType type;
    int lineNumber;
  public:
    Token(std::string newLexeme, tokenType newType, int newLineNumber) {
      this->lexeme = newLexeme;
      this->type = newType;
      this->lineNumber = newLineNumber;
    }

    std::string getLexeme() { return lexeme; }

    tokenType getTokenType() { return type; }

    int getLineNumber() { return lineNumber; }

    void setLexeme(std::string newLexeme) { lexeme = newLexeme; }

    void setType(tokenType newType) { type = newType; }

    void setLineNumber(int newLineNumber) { lineNumber = newLineNumber; }

    std::string toString() {
      std::string typeString;
      switch (type) {
        case keyword : typeString = "keyword"; break;
        case identifier : typeString = "identifier"; break;
        case symbol : typeString = "symbol"; break;
        case integer : typeString = "integer"; break;
        case stringConstant : typeString = "stringConstant"; break;
        case endOfFile : typeString = "endOfFile"; break;
      }
      return ("<" + lexeme + ", " + typeString + ", " + std::to_string(lineNumber) + ">");
    }
  };
}
