#include "Token.h"
// #include "Lexer.h"
#include "Parser.h"
#include <iostream>
#include <string>
#include <list>
#include "dirent.h"

// using namespace std;

int main(int argc, char **argv) {
  if (argc == 1) {
    // If a filename has not been inputted.
    std::cout << "Please enter a folder name!\nFor example: ./myjc *folder_name*\n";
    exit(0);
  }
  std::string folder(argv[1]);


  DIR *dir;
  struct dirent *ent;
  int i = 0;
  if ((dir = opendir(folder.c_str())) != NULL) {
    std::cout << "\n";

    // Create list of names of classes.
    std::list<std::string> listOfClasses;
    while ((ent = readdir (dir)) != NULL) {
      if (i > 1) {
        std::string filename(ent->d_name);
        if (filename.substr((filename.length() - 5), 5) == ".jack") {
          listOfClasses.push_back(filename);
        }
      }
      ++i;
    }

    i = 0;
    dir = opendir(folder.c_str());
    while ((ent = readdir (dir)) != NULL) {
      if (i > 1) {
        std::string filename(ent->d_name);
        if (filename.substr((filename.length() - 5), 5) == ".jack") {
          std::cout << "Compiling source code file: " << filename << " ... ";

          Parser::Parser *parser = new Parser::Parser();
          parser->init();
          parser->parseFile(folder + "/" + filename, listOfClasses);

          std::cout << "DONE\n";
        }
      }
      ++i;
    }
    std::cout << "\n";
  } else {
    std::cout << "Could not open directory: " << folder << "\n";
  }




}
