#include <list>
#include <iostream>
// using namespace std;
namespace Parser {
  enum symbolKind {var, staticVariable, field, argument, method, function, constructor, classDeclaration};
  class Symbol {
  private:
    symbolKind kind;
    std::string name;
    std::string type;
    int offset;
    int noArguments;
    std::string parentClass;
    bool initialised;
  public:
    Symbol(symbolKind kind, std::string name, std::string type, int offset, std::string parentClass) {
      this->kind = kind;
      this->name = name;
      this->type = type;
      this->offset = offset;
      this->noArguments = -1;
      this->parentClass = parentClass;
      this->initialised = false;
    }

    Symbol(symbolKind kind, std::string name, int offset, int noArguments, std::string returnType, std::string parentClass) {
      this->kind = kind;
      this->name = name;
      this->type = returnType;
      this->offset = offset;
      this->noArguments = noArguments;
      this->parentClass = parentClass;
      this->initialised = true;
    }

    symbolKind getKind() { return this->kind; }

    std::string getName() { return this->name; }

    std::string getType() { return this->type; }

    int getOffset() { return this->offset; }

    int getNoArguments() { return this->noArguments; }

    std::string getParentClass() { return this->parentClass; }

    void setInitialised() { this->initialised = true; }

    bool getInitialised() { return this->initialised; }

    std::string toString() {
      std::string kindString;
      if (kind == symbolKind(var)) {
        kindString = "var";
      } else if (kind == symbolKind(staticVariable)) {
        kindString = "static";
      } else if (kind == symbolKind(field)) {
        kindString = "field";
      } else if (kind == symbolKind(argument)) {
        kindString = "argument";
      } else if (kind == symbolKind(method)) {
        kindString = "method";
      } else if (kind == symbolKind(function)) {
        kindString = "function";
      } else if (kind == symbolKind(constructor)) {
        kindString = "constructor";
      } else if (kind == symbolKind(classDeclaration)) {
        kindString = "class";
      }
      if (noArguments == -1) {
        return "Symbol name: " + name + " - Type: " + type + " - Kind: " + kindString + " - Parent class: " + parentClass;
      } else {
        return "Subroutine name: " + name + " - Kind: " + kindString + " - Number of Arguments: " + std::to_string(noArguments) + " - Return Type: " + type + " - Parent class: " + parentClass;
      }
    }
  };

  class SymbolTable {
  private:
    std::list<Symbol> table;
    int localCount;
    int argumentCount;
    int fieldCount;
    int staticCount;
  public:
    SymbolTable() {
      localCount = 0;
      argumentCount = 0;
      fieldCount = 0;
      staticCount = 0;
    }

    void init() {
      localCount = 0;
      argumentCount = 0;
      fieldCount = 0;
      staticCount = 0;
    }

    void addSymbol(std::string name, std::string type, symbolKind kind, std::string parentClass) {
      std::list<Symbol>::iterator it;
      it = table.end();
      Symbol newSymbol = Symbol(kind, name, type, localCount, parentClass);
      if (kind == symbolKind(argument)) {
        if (kind == symbolKind(method)) {
          newSymbol = Symbol(kind, name, type, argumentCount + 1, parentClass);
        } else {
          newSymbol = Symbol(kind, name, type, argumentCount, parentClass);
        }

        argumentCount++;
      } else if (kind == symbolKind(field)) {
        newSymbol = Symbol(kind, name, type, fieldCount, parentClass);
        fieldCount++;
      } else if (kind == symbolKind(staticVariable)) {
        newSymbol = Symbol(kind, name, type, staticCount, parentClass);
        staticCount++;
      } else {
        localCount++;
      }
      table.insert(it, newSymbol);
    }

    void addSubroutine(std::string name, symbolKind kind, int noArguments, std::string returnType, std::string parentClass) {
      std::list<Symbol>::iterator it;
      it = table.end();
      Symbol newSymbol = Symbol(kind, name, localCount, noArguments, returnType, parentClass);
      table.insert(it, newSymbol);
      localCount++;
    }

    bool isChildOf(std::string child, std::string parent) {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getParentClass() == parent) {
          return true;
        }
      }
      return false;
    }

    int findSymbol(std::string name, std::string parentClass) {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if ((currentSymbol.getName() == name) && ((currentSymbol.getParentClass() == parentClass) || (currentSymbol.getParentClass() == "global"))) {
          if ((currentSymbol.getKind() != symbolKind(method)) && (currentSymbol.getKind() != symbolKind(function)) && (currentSymbol.getKind() != symbolKind(constructor))) {
            return 1;
          }
        }
      }
      return 0;
    }

    int findSymbol(std::string name) {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getName() == name) {
          return 1;
        }
      }
      return 0;
    }

    int findSubRoutine(std::string name, std::string parentName) {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getName() == name) {
          if (currentSymbol.getParentClass() == parentName) {
            return 1;
          }
        }
      }
      return 0;
    }

    bool isInitialised(std::string variableName) {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getName() == variableName) {
          if (currentSymbol.getInitialised() == true) {
            return true;
          } else {
            return false;
          }
        }
      }
      return false;
    }

    void setInitialised(std::string variableName) {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getName() == variableName) {
          it->setInitialised();
        }
      }
    }

    Symbol getSymbol(std::string symbolName) {
      Symbol returnSymbol = Symbol(symbolKind(field), "empty", "empty", -1, "empty");
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getName() == symbolName) {
          returnSymbol = currentSymbol;
          break;
        }
      }
      return returnSymbol;
    }

    Symbol getSubRoutine(std::string subroutineName, std::string parentName) {
      Symbol returnSymbol = Symbol(symbolKind(field), "empty", "empty", -1, "empty");
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        if (currentSymbol.getName() == subroutineName) {
          if (currentSymbol.getParentClass() == parentName) {
            returnSymbol = currentSymbol;
            break;
          }
        }
      }
      return returnSymbol;
    }

    void print() {
      for (std::list<Symbol>::iterator it = table.begin(); it !=table.end(); it++) {
        Symbol currentSymbol = *it;
        std::cout << currentSymbol.toString();
        std::cout << "\n";
      }
    }
  };
}
